package model;

import java.util.Vector;

public class Attempt {

	// Attribute
	private long gameId;
	private int P_test_id;
	private String first_name;
	private String sur_name;
	private int nummerierung;
	private double hitsShots;
	private double hitsTargets;
	private double reactionDuration;
	private double highscoreFormula;
	private String date;	
	private String time;	

	// Konstruktor
	public Attempt(int nummerierung, int P_test_id, double hitsShots, double hitsTargets, double reactionDuration,
			double highscoreFormula, String first_name, String sur_name, String date, String time) {
		super();
		this.nummerierung = nummerierung;
		this.P_test_id = P_test_id;
		this.reactionDuration = reactionDuration;
		this.highscoreFormula = highscoreFormula;
		this.first_name = first_name;
		this.sur_name = sur_name;
		this.hitsShots = hitsShots;
		this.hitsTargets = hitsTargets;
		this.date = date;
		this.time = time;
		
	}

	public Vector<String> getRowVector() {
		Vector<String> v = new Vector<String>(8);
		v.addElement(String.valueOf(this.nummerierung));
		v.addElement(this.first_name + " " + this.sur_name);
		v.addElement(this.date);
		v.addElement(this.time);
		v.addElement(String.valueOf((int) this.hitsTargets));
		v.addElement(String.valueOf((int) this.hitsShots));
		v.addElement(String.valueOf((int) this.reactionDuration));
		v.addElement(String.valueOf((int) this.highscoreFormula));
		return v;
	}

	
	
}
