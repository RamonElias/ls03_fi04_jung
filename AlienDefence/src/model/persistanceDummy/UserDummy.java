package model.persistanceDummy;

import java.sql.Date;
import java.time.LocalDate;

import model.User;
import model.persistance.IUserPersistance;

/**
 * Dummyklasse zum Testen
 * @author Tim Tenbusch
 *
 */
public class UserDummy implements IUserPersistance {

	public User readUser(String username) {
		return new User(1, "Dummy", "Persistenz", LocalDate.now(), "Dummystr.", "12C", "11111", "Nowhere", username, "pass", 12000, "gefangen", 1.58);
	}

	public int createUser(User user) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void updateUser(User user) {
		// TODO Auto-generated method stub
		
	}

	public void deleteUser(User user) {
		// TODO Auto-generated method stub
		
	}
	
}
