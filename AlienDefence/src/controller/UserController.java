package controller;

import java.time.LocalDate;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;
import model.persistanceDB.AccessDB;
import model.persistanceDB.PersistanceDB;
import model.persistanceDB.UserDB;

/**
 * controller for users
 * @author Frederic Jung
 */
public class UserController {
	
	private IUserPersistance userPersistance;
	
//TODO Persitenz aus DB ziehen
		//PersistanceDB db = new PersistanceDB();
		//IUserPersistance userDB = db.getUserPersistance();
		//UserController usC = new UserController(userDB);

	/**
	 * Construktor des UserControllers �ber die UserPersistance
	 * @param userPersistance Auslesen der persitenten Datens�tze der User
	 */
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	/**
	 * Construktor des UserControllers �ber die gesamten Persot�nten Datens�tze
	 * @param alienDefenceModel gesammte Persist�nz
	 */
	public UserController(IPersistance alienDefenceModel) {
		this.userPersistance = alienDefenceModel.getUserPersistance();
	}
	
	/**
	 * Erstellen eines neuen Users wenn dieser nochnciht in der DB vorhanden ist
	 * @param user Der neu anzulegende User
	 */
	public void createUser(User user) {
		if (this.userPersistance.readUser(user.getLoginname()) == null) {
			this.userPersistance.createUser(user);
		}
		else {
			System.out.println("User bereits vorhanden!");
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return this.userPersistance.readUser(username);
	}
	
	/**
	 * Die Datens�tze eines Users in der DB �berschreiben
	 * @param user Das neue User Objekt, dessen Daten die alten �berschreiben sollen
	 */
	public void changeUser(User user) {
		this.userPersistance.updateUser(user);
	}
	
	/**
	 * Der gesamte Datensatz eines Users soll aus der DB entfernt werden
	 * @param user Der zu l�schende User
	 */
	public void deleteUser(User user) {
		this.userPersistance.deleteUser(user);
	}
	
	/**
	 * Check ob das Passswort in der DB zu dem Angegebenen username/loginname passt
	 * @param username der zu checkende username/loginname
	 * @param passwort das vermeintliche passwort des Users
	 * @return boolean, wenn das Passwort �bereinstimmt 1, sonst 0
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = readUser(username, passwort);
		return user.getPassword().equals(passwort);
	}
}
