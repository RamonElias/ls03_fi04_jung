package controller;

import java.util.Vector;

import model.Level;
import model.persistance.IAttemptPersistance;
import model.persistance.IPersistance;

public class AttemptController {

	private IAttemptPersistance attemptPersistance;

	/**
	 * erstellt ein neues Objekt eines AttemptController welches Attemptobjekte in
	 * der übergebenen Datenhaltung persisiert
	 * 
	 * @param alienDefenceModel.getAttemptDB()
	 *            Persistenzklasse der Attemptobjekte
	 */
	public AttemptController(IPersistance alienDefenceModel) {
		this.attemptPersistance = alienDefenceModel.getAttemptPersistance();
	}

	public Vector<Vector<String>> getAllAttemptsPerLevel(int level_id, int game_id) {
		return attemptPersistance.getAllAttemptsPerLevel(level_id, game_id);
	}

	public int getPlayerPosition() {
		return attemptPersistance.getPlayerPosition();
	}

	public void deleteHighscore(int level_id) {
		attemptPersistance.deleteHighscore(level_id);
	}

	/**
	 * 
	 * @param level Levelobjekt
	 * @param hitcounter Controllerobjekt das die Treffer und Reaktionszeiten misst
	 * @return points 
	 */
	public int calculatePoint(Level level, HitCounter hitcounter) {
		long trefferZielRatio = hitcounter.getHit() / level.getTargets().size();
		long trefferSchuesseRatio = hitcounter.getHit() / hitcounter.getShots();
		long prezision = hitcounter.getReactionTime() / hitcounter.getSumReactionDiffernce();
		
		return (int) ((trefferZielRatio * 400) + (trefferSchuesseRatio * 200) + (1000 - (prezision * 400)));
	}
}
