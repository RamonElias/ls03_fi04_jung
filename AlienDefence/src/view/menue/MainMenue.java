package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import model.User;
import model.persistanceDB.PersistanceDB;

public class MainMenue extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfdLogin;
	private JPasswordField pfdPassword;

	/**
	 * Create the frame.
	 */
	public MainMenue(AlienDefenceController alienDefenceController) {
		setMinimumSize(new Dimension(600, 620));
		
		// Allgemeine JFrame-Einstellungen
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setUndecorated(true);
		setVisible(true);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);

		JPanel pnlTop = new JPanel();
		pnlTop.setBackground(Color.BLACK);
		contentPane.add(pnlTop, BorderLayout.NORTH);
		pnlTop.setLayout(new BorderLayout(0, 0));

		// �berschrift
		JLabel lblHeadline = new JLabel("ALIEN DEFENCE");
		pnlTop.add(lblHeadline, BorderLayout.NORTH);
		lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeadline.setForeground(new Color(124, 252, 0));
		lblHeadline.setFont(new Font("Yu Gothic UI", Font.BOLD, 25));

		Component verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setPreferredSize(new Dimension(0, 200));
		pnlTop.add(verticalStrut, BorderLayout.CENTER);
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("./pictures/logo.png").getImage().getScaledInstance(140, 140, Image.SCALE_DEFAULT));

		JPanel pnlAll = new JPanel();
		pnlAll.setBackground(Color.BLACK);
		contentPane.add(pnlAll, BorderLayout.CENTER);
		pnlAll.setLayout(new BorderLayout(0, 0));

		// Logo
		JPanel pnlLogo = new JPanel();
		pnlAll.add(pnlLogo, BorderLayout.NORTH);
		pnlLogo.setBackground(Color.BLACK);

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(imageIcon);
		pnlLogo.add(lblLogo);

		JPanel pnlCenter = new JPanel();
		pnlAll.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setBackground(Color.BLACK);

		// Alles unter dem Bild, Tabellenlayout mit einer Spalte
		JPanel pnlButtons = new JPanel();
		pnlCenter.add(pnlButtons);
		pnlButtons.setBackground(Color.BLACK);
		pnlButtons.setLayout(new GridLayout(0, 1, 0, 0));

		// Nutzername
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setForeground(Color.orange);

		tfdLogin = new JTextField();
		tfdLogin.setColumns(10);

		// Passwort
		JLabel lblPassword = new JLabel("Passwort:");
		lblPassword.setForeground(Color.orange);

		pfdPassword = new JPasswordField();

		// Spiel starten
		// Die Textfelder werden ausgewertet und das Passwort validiert, dann wird das
		// Spiel gestartet
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSpielen_Clicked(alienDefenceController, "spielen");
				setVisible(false);
			}
		});

		Box horizontalBox = Box.createHorizontalBox();

		JButton btnTesten = new JButton("Testen");
		btnTesten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LeveldesignWindow(alienDefenceController, "spielen");
				setVisible(false);
			}
		});

		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// new Highscore(alienDefenceController.getAttemptController(),
				// cboLevelChooser.getSelectedIndex());
				new LeveldesignWindow(alienDefenceController, "highscore");
				setVisible(false);
			}
		});

		JButton btnLeveleditor = new JButton("Leveleditor");
		btnLeveleditor.setBackground(Color.ORANGE);
		btnLeveleditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LeveldesignWindow(alienDefenceController, "editor");
				setVisible(false);
			}
		});
		
		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.setBackground(Color.GRAY);
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		//Reihenfolge der Buttons
		pnlButtons.add(lblLogin);
		pnlButtons.add(tfdLogin);
		pnlButtons.add(lblPassword);
		pnlButtons.add(pfdPassword);
		pnlButtons.add(horizontalBox);
		pnlButtons.add(btnSpielen);
		pnlButtons.add(btnTesten);
		pnlButtons.add(btnHighscore);
		pnlButtons.add(btnLeveleditor);
		pnlButtons.add(btnBeenden);
		
		pnlButtons.setPreferredSize(new Dimension(300, pnlButtons.getComponentCount()*30));
	}

	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, String btnConfig) {
		// User aus Datenbank holen
		// TODO B�ser Versto� gegen MVC - hier muss sp�ter nochmal nachgebessert werden
		User user = new PersistanceDB().getUserPersistance().readUser(tfdLogin.getText());

		// Spielstarten, wenn Nutzer existiert und Passwort �bereinstimmt
		if (user != null && user.getPassword().equals(new String(pfdPassword.getPassword()))) {

			Thread t = new Thread("GameThread") {
				@Override
				public void run() {
					// DONE
					new LeveldesignWindow(alienDefenceController, btnConfig);
				}
			};
			t.start();
		} else {
			// Fehlermeldung - Zugangsdaten fehlerhaft
			JOptionPane.showMessageDialog(null, "Zugangsdaten nicht korrekt", "Fehler", JOptionPane.ERROR_MESSAGE);
		}
	}
}
