package view.menue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.LevelController;
import controller.TargetController;
import model.Level;

@SuppressWarnings("serial")
public class LeveldesignWindow extends JFrame {

	private LevelController lvlControl;
	private JPanel contentPane;
	private LevelChooser cardChooseLevel;
	private LevelEditor cardLevelEditor;

	private CardLayout cards;

	/**
	 * Create the frame.
	 */
	public LeveldesignWindow(AlienDefenceController alienDefenceController, String btnConfig) {
		this.lvlControl = alienDefenceController.getLevelController();

		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setUndecorated(true);
		setVisible(true);
		
		setTitle("ALIEN DEFENCE");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		this.cards = new CardLayout();
		contentPane.setLayout(cards);

		this.cardChooseLevel = new LevelChooser(alienDefenceController, this, btnConfig);
		contentPane.add(cardChooseLevel, "levelChooser");

		this.cardLevelEditor = new LevelEditor(this, alienDefenceController, Level.getDefaultLevel());
		contentPane.add(cardLevelEditor, "levelEditor");

		this.showLevelChooser();
		this.setVisible(true);
	}

	/**
	 * display leveleditor with a new level
	 */
	public void startLevelEditor() {
		this.cardLevelEditor.setLvl(this.lvlControl.createLevel());
		this.cards.show(contentPane, "levelEditor");
	}

	/**
	 * disply leveleditor with a new level
	 * 
	 * @param level_id
	 */
	public void startLevelEditor(int level_id) {
		this.cardLevelEditor.setLvl(this.lvlControl.readLevel(level_id));
		this.cards.show(contentPane, "levelEditor");
	}

	/**
	 * display a jTable with all Levels
	 */
	public void showLevelChooser() {
		this.cards.show(contentPane, "levelChooser");
		this.cardChooseLevel.updateTableData();
	}
	
	public void hideLevelChooser() {
		this.setVisible(false);
	}

}