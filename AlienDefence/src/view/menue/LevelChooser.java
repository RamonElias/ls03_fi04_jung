package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private AlienDefenceController alienDefenceController;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow,
			String btnConfig) {

		setBackground(new Color(0, 0, 0));
		this.alienDefenceController = alienDefenceController;
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JLabel lblLevelauswahl = new JLabel("LEVELAUSWAHL");
		lblLevelauswahl.setPreferredSize(new Dimension(76, 32));
		lblLevelauswahl.setForeground(new Color(124, 252, 0));
		lblLevelauswahl.setFont(new Font("Yu Gothic UI", Font.BOLD, 25));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		add(panel, BorderLayout.SOUTH);

		JPanel pnlButtons = new JPanel();
		panel.add(pnlButtons);
		pnlButtons.setBackground(new Color(0, 0, 0));

		pnlButtons.setLayout(new GridLayout(0, 1, 0, 0));

		// Wenn der Button Spielen geklickt wird, soll auch nur btnSpielen auswählbar
		// sein

		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSpielen_Clicked();
			}
		});

		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHighscore_Clicked();
			}
		});

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});

		JButton btnZurueck = new JButton("Hauptmen\u00FC");
		btnZurueck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnZurueck_Clicked();
			}
		});

		// Einblenden der Buttons, passend zu dem im MainMenu gedrückten Button
		if (btnConfig == "spielen") {
			pnlButtons.add(btnSpielen);
			pnlButtons.add(btnHighscore);
		} else if (btnConfig == "editor") {
			pnlButtons.add(btnUpdateLevel);
			pnlButtons.add(btnNewLevel);
			pnlButtons.add(btnDeleteLevel);
		} else if (btnConfig == "highscore") {
			pnlButtons.add(btnHighscore);
		}
		pnlButtons.add(btnZurueck);

		// Zum darstellen der Buttons gleichgroß wie im MainMenu werden die Buttons
		// dynamisch angepasst
		pnlButtons.setPreferredSize(new Dimension(240, pnlButtons.getComponentCount() * 30));

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setForeground(Color.WHITE);
		spnLevels.setBackground(new Color(0, 0, 0));
		add(spnLevels, BorderLayout.CENTER);

		// ROWS - Die einstellungen der dynamisch eingereihten Level
		tblLevels = new JTable();
		tblLevels.setRowHeight(30);
		tblLevels.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tblLevels.setSelectionBackground(Color.WHITE);
		tblLevels.setGridColor(Color.WHITE);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblLevels.setForeground(Color.white);
		spnLevels.setViewportView(tblLevels);
		spnLevels.getViewport().setBackground(Color.black);

		// HEADER - Die einstellungen der reihen Bezeichnungen
		tblLevels.getTableHeader().setBackground(Color.black);
		tblLevels.getTableHeader().setForeground(Color.ORANGE);
		tblLevels.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 16));

		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor(getSelected_levelId());
	}

	public void btnDeleteLevel_Clicked() {
		this.lvlControl.deleteLevel(getSelected_levelId());
		this.updateTableData();
	}

	public void btnHighscore_Clicked() {
		try {
			new Highscore(alienDefenceController.getAttemptController(), getSelected_levelId());
		} catch(ArrayIndexOutOfBoundsException ex) {}
	}

	public void btnZurueck_Clicked() {
		new MainMenue(alienDefenceController);
		leveldesignWindow.hideLevelChooser();
	}

	public void btnSpielen_Clicked() {

		// Erstellt Modell von aktuellen Nutzer
		User user = new User(1, "test", "pass");
		Level lvl = this.lvlControl.readLevel(getSelected_levelId());

		Thread t = new Thread("GameThread") {

			@Override
			public void run() {

				GameController gameController = alienDefenceController.startGame(lvl, user);
				new GameGUI(gameController).start();

			}
		};
		t.start();
	}

	private int getSelected_levelId() {
		return Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
	}
}
