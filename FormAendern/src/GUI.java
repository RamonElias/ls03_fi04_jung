import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtEingabefeld;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 811);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.background"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblText = new JLabel("Hier k\u00F6nnte Ihre Werbung stehen!");
		lblText.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblText.setHorizontalAlignment(SwingConstants.CENTER);
		lblText.setBounds(12, 13, 439, 102);
		contentPane.add(lblText);
		
		JLabel lblAufgabeHintergrundfarge = new JLabel("Aufgabe 1: Hintergrundfarge");
		lblAufgabeHintergrundfarge.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeHintergrundfarge.setBounds(12, 123, 408, 24);
		contentPane.add(lblAufgabeHintergrundfarge);
		
		JButton btnRot = new JButton("Rot");
		btnRot.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(12, 160, 139, 44);
		contentPane.add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(314, 160, 139, 44);
		contentPane.add(btnBlau);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(163, 160, 139, 44);
		contentPane.add(btnGrn);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(12, 217, 139, 44);
		contentPane.add(btnGelb);
		
		JButton btnStandartfarbe = new JButton("Standartfarbe");
		btnStandartfarbe.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnStandartfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandartfarbe.setBounds(163, 217, 139, 44);
		contentPane.add(btnStandartfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe W\u00E4hlen");
		btnFarbeWhlen.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color ausgewaehlteFarbe = JColorChooser.showDialog(null, 
			            "Farbauswahl", null);
				contentPane.setBackground(ausgewaehlteFarbe);
			}
		});
		btnFarbeWhlen.setBounds(314, 217, 139, 44);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text Formatieren");
		lblAufgabeText.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeText.setBounds(12, 274, 408, 24);
		contentPane.add(lblAufgabeText);
		
		JButton btnAriel = new JButton("Ariel");
		btnAriel.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnAriel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnAriel.setBounds(12, 311, 139, 44);
		contentPane.add(btnAriel);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSansMs.setBounds(163, 311, 139, 44);
		contentPane.add(btnComicSansMs);
		
		JButton btnTimesNewRoman = new JButton("Times new Roman");
		btnTimesNewRoman.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTimesNewRoman.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Times New Roman", Font.PLAIN, 12));
			}
		});
		btnTimesNewRoman.setBounds(314, 311, 139, 44);
		contentPane.add(btnTimesNewRoman);
		
		txtEingabefeld = new JTextField();
		txtEingabefeld.setFont(new Font("Tahoma", Font.BOLD, 13));
		txtEingabefeld.setText("Schreiben Sie Ihre eigene Werbeanzeige!");
		txtEingabefeld.setBounds(12, 361, 441, 22);
		contentPane.add(txtEingabefeld);
		txtEingabefeld.setColumns(10);
		
		JButton btnTextPaste = new JButton("Text \u00DCbertragen");
		btnTextPaste.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTextPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText(txtEingabefeld.getText());
			}
		});
		btnTextPaste.setBounds(12, 396, 211, 25);
		contentPane.add(btnTextPaste);
		
		JButton btnTextDelete = new JButton("Text L\u00F6schen");
		btnTextDelete.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTextDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText("");
			}
		});
		btnTextDelete.setBounds(242, 396, 211, 25);
		contentPane.add(btnTextDelete);
		
		JLabel lblAufgabeSchirftfarbe = new JLabel("Aufgabe 3: Schirftfarbe \u00E4ndern");
		lblAufgabeSchirftfarbe.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeSchirftfarbe.setBounds(12, 434, 408, 24);
		contentPane.add(lblAufgabeSchirftfarbe);
		
		JButton btnTextColRed = new JButton("Rot");
		btnTextColRed.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTextColRed.setForeground(Color.BLACK);
		btnTextColRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.RED);
			}
		});
		btnTextColRed.setBounds(12, 471, 139, 44);
		contentPane.add(btnTextColRed);
		
		JButton btnTextColWhite = new JButton("Wei\u00DF");
		btnTextColWhite.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTextColWhite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.WHITE);
			}
		});
		btnTextColWhite.setBounds(163, 471, 139, 44);
		contentPane.add(btnTextColWhite);
		
		JButton btnTextColBlack = new JButton("Schwarz");
		btnTextColBlack.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTextColBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLACK);
			}
		});
		btnTextColBlack.setBounds(314, 471, 139, 44);
		contentPane.add(btnTextColBlack);
		
		JLabel lblAufgabeSchriftge = new JLabel("Aufgabe 4: Schriftg\u00F6\u00DFe \u00E4ndern");
		lblAufgabeSchriftge.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeSchriftge.setBounds(12, 528, 408, 24);
		contentPane.add(lblAufgabeSchriftge);
		
		JButton button = new JButton("+");
		button.setFont(new Font("Tahoma", Font.BOLD, 13));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String fontType = lblText.getFont().getFamily();
				int fontSize = lblText.getFont().getSize();
				lblText.setFont(new Font(fontType, Font.PLAIN, fontSize + 1));
			}
		});
		button.setBounds(12, 565, 211, 25);
		contentPane.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String fontType = lblText.getFont().getFamily();
				int fontSize = lblText.getFont().getSize();
				lblText.setFont(new Font(fontType, Font.PLAIN, fontSize - 1));
			}
		});
		button_1.setBounds(240, 565, 211, 25);
		contentPane.add(button_1);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeTextausrichtung.setBounds(12, 603, 408, 24);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbuending = new JButton("Linksbuending");
		btnLinksbuending.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLinksbuending.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbuending.setForeground(Color.BLACK);
		btnLinksbuending.setBounds(12, 640, 139, 44);
		contentPane.add(btnLinksbuending);
		
		JButton btnZentriert = new JButton("Zentriert");
		btnZentriert.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(163, 640, 139, 44);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbuendig = new JButton("Rechtsbuendig");
		btnRechtsbuendig.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRechtsbuendig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbuendig.setBounds(314, 640, 139, 44);
		contentPane.add(btnRechtsbuendig);
		
		JLabel lblAufgabeProgram = new JLabel("Aufgabe 6: Program beenden");
		lblAufgabeProgram.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeProgram.setBounds(12, 697, 408, 24);
		contentPane.add(lblAufgabeProgram);
		
		JButton btnX = new JButton("x");
		btnX.setForeground(Color.BLACK);
		btnX.setBackground(new Color(255, 0, 0));
		btnX.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnX.setBounds(12, 726, 439, 25);
		contentPane.add(btnX);
	}
}
